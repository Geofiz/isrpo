﻿using Models;
using Services.Dtos;

namespace Services.Interfaces;

public interface IAutopartService
{
    void Create(string name, string carmodel, string carbrand, decimal price, int quantity, string provider);
    List<ReadAutopartDto> Read();
    ReadAutopartDto? Read(int id);
    bool Update(int id, string name, string carmodel, string carbrand, decimal price, int quantity, string provider);
    bool Delete(int id);
}