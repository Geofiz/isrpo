﻿using App.Metrics;
using App.Metrics.Counter;

namespace Services;

public class MetricsRegistry
{
    public static CounterOptions CreatedAutopartsCounter => new CounterOptions
    {
        Name = "Created Autoparts",
        Context = "AutopartsApi",
        MeasurementUnit = Unit.Calls
    };
    
    public static CounterOptions ReadAutopartsCounter => new CounterOptions
    {
        Name = "Read Autoparts",
        Context = "AutopartsApi",
        MeasurementUnit = Unit.Calls
    };
    
    public static CounterOptions UpdatedAutopartsCounter => new CounterOptions
    {
        Name = "Updated Autoparts",
        Context = "AutopartsApi",
        MeasurementUnit = Unit.Calls
    };
    
    public static CounterOptions DeletedAutopartsCounter => new CounterOptions
    {
        Name = "Deleted Autoparts",
        Context = "AutopartsApi",
        MeasurementUnit = Unit.Calls
    };
}