﻿using App.Metrics;
using DAL;
using Models;
using Services.Dtos;
using Services.Interfaces;

namespace Services.Realization;

public class AutopartService : IAutopartService
{
    private AutopartStoreDbContext _dbContext;
    private readonly IMetrics _metrics;

    public AutopartService(AutopartStoreDbContext dbContext, IMetrics metrics)
    {
        _dbContext = dbContext;
        _metrics = metrics;
    }
    
    public void Create(string name, string carmodel, string carbrand, decimal price, int quantity, string provider)
    {
        var autopart = new Autopart(){Name = name, CarModel = carmodel, CarBrand = carbrand, Price = price, Quantity = quantity, Provider = provider};
        _dbContext.Autoparts.Add(autopart);
        _dbContext.SaveChanges();
        _metrics.Measure.Counter.Increment(MetricsRegistry.CreatedAutopartsCounter);
    }

    public List<ReadAutopartDto> Read()
    {
        _metrics.Measure.Counter.Increment(MetricsRegistry.ReadAutopartsCounter);
        return _dbContext.Autoparts.Select(x => new ReadAutopartDto(x)).ToList();
    }

    public ReadAutopartDto? Read(int id)
    {
        var autopart = _dbContext.Autoparts.FirstOrDefault(x => x.Id == id);
        _metrics.Measure.Counter.Increment(MetricsRegistry.ReadAutopartsCounter);
        return (autopart != null) ? new ReadAutopartDto(autopart) : null;
    }
    
    public bool Update(int id, string name, string carmodel, string carbrand, decimal price, int quantity, string provider)
    {
        var autopart = _dbContext.Autoparts.FirstOrDefault(x => x.Id == id);
        
        if (autopart == null) return false;

        autopart.Name = name;
        autopart.CarBrand = carbrand;
        autopart.CarModel = carmodel;
        autopart.Price = price;
        autopart.Quantity = quantity;
        autopart.Provider = provider;
        
        _dbContext.SaveChanges();
        _metrics.Measure.Counter.Increment(MetricsRegistry.UpdatedAutopartsCounter);
        return true;
    }
    
    public bool Delete(int id)
    {
        var autopart = _dbContext.Autoparts.FirstOrDefault(x => x.Id == id);
        
        if (autopart == null) return false;
        
        _dbContext.Autoparts.Remove(autopart);
        _dbContext.SaveChanges();
        _metrics.Measure.Counter.Increment(MetricsRegistry.DeletedAutopartsCounter);
        return true;
    }
}