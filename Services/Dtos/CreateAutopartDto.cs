﻿namespace Services.Dtos;

public class CreateAutopartDto
{
    public string Name { get; set; }
    public string CarBrand { get; set; }
    public string CarModel { get; set; }
    public decimal Price { get; set; }
    public int Quantity { get; set; }
    public string Provider { get; set; }

    public CreateAutopartDto()
    {
        
    }

    public CreateAutopartDto(string name, string carbrand, string carmodel, decimal price, int quantity, string provider)
    {
        Name = name;
        CarBrand = carbrand;
        CarModel = carmodel;
        Price = price;
        Quantity = quantity;
        Provider = provider;
    }
}