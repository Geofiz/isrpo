﻿namespace Services.Dtos;

public class ReadAutopartDto : CreateAutopartDto
{
    public int Id { get; set; }

    public ReadAutopartDto()
    {
        
    }

    public ReadAutopartDto(int id, string name, string carmodel, string carbrand, decimal price, int quantity, string provider) : base(name, carmodel, carbrand, price, quantity, provider)
    {
        Id = id;
    }

    public ReadAutopartDto(Models.Autopart autopart) : this(autopart.Id, autopart.Name, autopart.CarModel, autopart.CarBrand, autopart.Price, autopart.Quantity, autopart.Provider)
    {
        
    }
}