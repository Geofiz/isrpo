﻿using System.ComponentModel.DataAnnotations;

namespace Models;

public class Autopart
{
    [Key]
    public int Id { get; set; }
    public string Name { get; set; }
    public string CarBrand { get; set; }
    public string CarModel { get; set; }
    public decimal Price { get; set; }
    public int Quantity { get; set; }
    public string Provider { get; set; }
}