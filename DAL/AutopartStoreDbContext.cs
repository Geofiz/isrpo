﻿using Microsoft.EntityFrameworkCore;
using Models;

namespace DAL;

public class AutopartStoreDbContext : DbContext
{
    public AutopartStoreDbContext()
    {
        
    }

    public AutopartStoreDbContext(DbContextOptions<AutopartStoreDbContext> options) : base(options)
    {
        
    }

    public DbSet<Autopart> Autoparts { get; set; } = null!;
    
    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        if (!optionsBuilder.IsConfigured)
        {
            optionsBuilder.UseNpgsql("User ID=postgres;Password=postgres;Host=localhost;Port=5432;Database=brichka;");
        }
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
    }
}