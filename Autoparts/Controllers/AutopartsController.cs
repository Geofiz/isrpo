﻿using Microsoft.AspNetCore.Mvc;
using Models;
using Services.Dtos;
using Services.Interfaces;

namespace Autoparts.Controllers;

[Route("api/[controller]")]
[ApiController]
public class AutopartsController : ControllerBase
{
    private IAutopartService _autopartService;

    public AutopartsController(IAutopartService autopartService)
    {
        _autopartService = autopartService;
    }

    [HttpPost]
    public void Create([FromBody]CreateAutopartDto dto)
    {
        _autopartService.Create(dto.Name, dto.CarModel, dto.CarBrand, dto.Price, dto.Quantity, dto.Provider);
    }

    [HttpGet]
    public IEnumerable<ReadAutopartDto> Read()
    {
        return _autopartService.Read();
    }

    [HttpGet("{id}")]
    public IActionResult Read([FromRoute] int id)
    {
        var autopart = _autopartService.Read(id);
        return (autopart != null) ? Ok(autopart) : NotFound();
    }
    
    [HttpPut("{id}")]
    public IActionResult Update([FromRoute]int id, [FromBody]CreateAutopartDto dto)
    {
        var result = _autopartService.Update(id, dto.Name, dto.CarBrand, dto.CarModel, dto.Price, dto.Quantity,
            dto.Provider);
        return (result) ? Ok(result) : BadRequest("Автозапчасти с таким идентификатором не существует");
    }
    
    [HttpDelete("{id}")]
    public IActionResult Delete([FromRoute]int id)
    {
        var result = _autopartService.Delete(id);
        return (result) ? Ok(result) : BadRequest("Автозапчасти с таким идентификатором не существует");
    }
}